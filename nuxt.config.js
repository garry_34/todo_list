module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'todo',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '{{escape description }}' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'}
    ]
  },
  modules: [
    'nuxt-vuex-localstorage'
  ],
  plugins: [  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  server: {
    port: 3008, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  buildModules: [
    // Simple usage
    // '@nuxtjs/vuetify'
  ],
  env: {
    apiUrl: process.env.API_URL || 'http://localhost:8000'
  },


  axios: {
    baseURL: 'http://foodway2cp.imb.plus/api'

  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

