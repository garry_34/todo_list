export const state = () => ({
  modal_triger: false,
  type_modal: '',
  index: undefined
})

export const mutations = {
  setModalTriger(state, payload) {
    state.modal_triger = payload
  },
  setIndex(state, payload){
    state.index = payload
  },
  setType(state, payload){
    state.type_modal = payload
  }
}
export const actions = {
  delItem({ rootGetters, commit, state}) {
    // Удаление задания из массива
   let vuexArr = rootGetters['localStorage/gettodoList']
    let copyArr = vuexArr.slice(0, vuexArr.length)
    copyArr.splice(state.index, 1)
    commit('localStorage/setList', copyArr, {root:true})
    commit('setModalTriger', false)
    commit('setIndex', undefined)
    this.$router.push('/')

  }
}
