export const state = () => ({
  todoList: []
})
export const mutations = {
  setList(state,payload){
    state.todoList = payload
  },
  setListItem(state, payload){
    state.todoList[payload.index] = payload.data
  }
}
