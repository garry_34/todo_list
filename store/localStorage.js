export const state = () => ({
  todoList: null
})
export const mutations = {
  setList(state,payload){
    state.todoList = payload
  }
}
export const getters = {
  gettodoList: (state) => state.todoList
}
