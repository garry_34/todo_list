import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify, {
  theme: {
    primary: '#46a928',
    secondary: '#46a928',
    accent: '#ff9f00',
    error: '#b71c1c'
  }
})
